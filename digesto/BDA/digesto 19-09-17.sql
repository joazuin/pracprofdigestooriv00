-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-09-2017 a las 23:57:37
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `digesto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro`
--

CREATE TABLE `libro` (
  `id_libro` int(3) NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `libro`
--

INSERT INTO `libro` (`id_libro`, `descripcion`) VALUES
(1, 'Infraestructura y Servicios'),
(2, 'Zonificación'),
(3, 'Comercio e Industrias'),
(4, 'Medio Ambiente'),
(5, 'Salubridad y Seguridad Pública'),
(6, 'Vía Pública'),
(7, 'Urbanismo'),
(8, 'Vivienda'),
(9, 'Educación Cultura Patrimonio Culto Turismo'),
(10, 'Gobierno Municipal'),
(11, 'Familia Discapacidad Ancianidad'),
(12, 'Recreación y Entretenimiento'),
(13, 'Tributos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenanza`
--

CREATE TABLE `ordenanza` (
  `nro_Ord` int(3) NOT NULL,
  `anio` year(4) NOT NULL,
  `sancion` date NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `expte` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `id_Tema` int(3) NOT NULL,
  `url` varchar(60) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ordenanza`
--

INSERT INTO `ordenanza` (`nro_Ord`, `anio`, `sancion`, `descripcion`, `expte`, `id_Tema`, `url`) VALUES
(12345, 2009, '2009-09-17', 'perritos calientes', '4578/2009', 5, 'pdf\\2.pdf'),
(23456, 2010, '2010-09-17', 'gatitos felices', '8974/2010', 1, 'pdf\\entradas.pdf'),
(34567, 2010, '2010-04-25', 'pericos locos', '2510/2010', 9, 'pdf\\Guardianes.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema`
--

CREATE TABLE `tema` (
  `id_Tema` int(3) NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `id_titulo` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tema`
--

INSERT INTO `tema` (`id_Tema`, `descripcion`, `id_titulo`) VALUES
(1, 'Agua Corriente', 1),
(2, 'Efluentes Cloacales', 1),
(3, 'Planta Potabilizadora', 1),
(4, 'Cementerios Municipales', 1),
(5, 'Cementerios Parque', 1),
(6, 'Salas de Velatorio', 1),
(7, 'Planta de Tratamientos de Residuos Sólidos y Urbanos', 1),
(8, 'Residuos Peligrosos', 1),
(9, 'Residuos Domiciliarios', 1),
(10, 'Residuos en la Vía Pública', 1),
(11, 'Arbolado Público', 1),
(12, 'Modificaciones al Código de Edificación', 2),
(13, 'Construcción Clandestina ', 2),
(14, 'Profesionales Intervinientes', 2),
(15, 'Sistemas Alternativos de Construcción', 2),
(16, 'Asentamientos Inestables', 2),
(17, 'Barrios y Loteos', 2),
(18, 'Loteos para Vivienda Social', 2),
(19, 'Terrenos Baldíos', 2),
(20, 'Obra Pública', 2),
(21, 'Obra Privada', 2),
(22, 'Energía Eléctrica', 2),
(23, 'Catastro', 2),
(24, 'Régimen del Agua', 3),
(25, 'Zona Residencial', 4),
(26, 'Zona Residencial Mixta', 4),
(27, 'Zona Especial Barrios', 4),
(28, 'Zona de Reserva Urbana Residencial', 5),
(29, 'Zona de Reserva Residencial Especial I', 5),
(30, 'Zona de Reserva Residencial Especial II', 5),
(31, 'Zona de Reserva Urbana Residencial Especial I', 5),
(32, 'Zona de Reserva Urbana Residencial Especial II', 5),
(33, 'Zona de Reserva Urbana Residencial Especial IV', 5),
(34, 'Zona de Reserva Urbana Inmediata', 5),
(35, 'Zona de Reserva Urbana Mediata', 5),
(36, 'Zona de Reserva Urbana Mediata Residencial Especial II', 5),
(37, 'Zona de Reserva Urbana Mediata Residencial Especial III', 5),
(38, 'Zona de Reserva Urbana Control Ambiental I', 5),
(39, 'Zona de Reserva Urbana Control Ambiental II', 5),
(40, 'Zona de Reserva Urbana Control Ambiental III', 5),
(41, 'Zona de Reserva Urbana Parque Metropolitano Sur', 5),
(42, 'Comercial', 6),
(43, 'Comercial Mixta I', 6),
(44, 'Comercial Mixta I A', 6),
(45, 'Comercial Mixta II', 6),
(46, 'Comercial Mixta II A', 6),
(47, 'Comercial Mixta III', 6),
(48, 'Zona Lineal de Servicios Rurales 500 m2', 7),
(49, 'Zona Lineal de Servicios Rurales 750 m2', 7),
(50, 'Zona de Desarrollo de Servicios 300 m2', 7),
(51, 'Zona de Desarrollo de Servicios 500 m2', 7),
(52, 'Zona de Desarrollo de Servicios 200 m2', 7),
(53, 'Zona de Desarrollo de Servicios Rurales 300 m2', 7),
(54, 'Zona Lineal de Servicios Rurales 300 m2', 7),
(55, 'Zona de Reserva Industrial 1500 m2', 8),
(56, 'Zona Industrial SubZona A 1500 m2', 8),
(57, 'Zona Industrial SubZona B 1500 m2', 8),
(58, 'Zona Industrial SubZona C 200 m2', 8),
(59, 'Parque Industrial', 8),
(60, 'Zona Industrial Carril Rodríguez Peña', 8),
(61, 'Talleres de Reparación y/o Elaboración', 8),
(62, 'Zona Productiva Agropecuaria Controlada', 9),
(63, 'Zona Rural', 9),
(64, 'Zona del Parque de Reserva Barrancas', 9),
(65, 'Conjuntos Habitacionales y Edilicios', 10),
(66, 'Conglomerados Urbanos de Carácter Rural', 10),
(67, 'Comisión Mixta de Zonificación', 11),
(68, 'Plan Estratégico', 11),
(69, 'Comercios y Centros Comerciales', 12),
(70, 'Hipermercados', 12),
(71, 'Ferias y Mercados Persas', 12),
(72, 'Expendedores de Combustibles', 12),
(73, 'Inmobiliarias y Casas de Remate', 13),
(74, 'Empacadoras y Productores', 13),
(75, 'Productoras Olivícolas', 13),
(76, 'Régimen de Promoción Industrial', 14),
(77, 'Parques Industriales', 14),
(78, 'Denominación de Origen', 14),
(79, 'Micro - Emprendimientos', 14),
(80, 'Comercios e Industrias', 15),
(81, 'Establecimientos Industriales', 16),
(82, 'Reglamentación de los Proyectos', 16),
(83, 'Acopio y Comercialización del Guano', 16),
(84, 'Actividades Avícolas y Pecuarias', 16),
(85, 'Explotación Hidrocarburífera', 16),
(86, 'Regulación Sanitaria', 17),
(87, 'Campañas Sanitarias', 17),
(88, 'Celebración de Convenios', 17),
(89, 'Instituciones Sanitarias ', 17),
(90, 'Infraestructura Sanitaria', 17),
(91, 'Denominación de los Centros de Salud', 17),
(92, 'Alarmas Comunitarias', 18),
(93, 'Antenas', 18),
(94, 'Geriátricos', 18),
(95, 'Bromatologia', 18),
(96, 'Bebidas Alcohólicas', 18),
(97, 'Pornografía', 18),
(98, 'Animales', 18),
(99, 'Convenios Declaraciones', 19),
(100, 'Cuerpo de Bomberos Voluntarios de Maipú', 19),
(101, 'Regulación', 20),
(102, 'Estacionamiento Vehicular', 21),
(103, 'Estacionamiento Medido', 21),
(104, 'Circulación Vehicular', 21),
(105, 'Refuncionalización del Tránsito', 21),
(106, 'Consejos Consultivos', 22),
(107, 'Ordenadores Viales', 22),
(108, 'Dirección de Tránsito y Prevención Ciudadana', 22),
(109, 'Regulación', 23),
(110, 'Regulación', 24),
(111, 'Parque y Forestación', 25),
(112, 'Viveros', 25),
(113, 'Plazas Plazoletas y Paseos', 25),
(114, 'Nominación', 26),
(115, 'Nominación', 27),
(116, 'Distritos', 28),
(117, 'División Política del Dpto.', 28),
(118, 'Límites Ejes Divisorios Numeración', 28),
(119, 'Señalización Cartelería Identificación', 28),
(120, 'Reglamentación', 29),
(121, 'Plan Fomento a la Vivienda', 30),
(122, 'Créditos y Beneficios', 30),
(123, 'Vivienda Social', 30),
(124, 'Vivienda en Estado de Abandono', 30),
(125, 'IPV', 30),
(126, 'Bibliotecas y Videotecas', 31),
(127, 'Convenios - Padrinazgos', 31),
(128, 'Equipamiento en Educación', 31),
(129, 'Salubridad en Educación', 31),
(130, 'Sistema de Becas', 31),
(131, 'Guarderías y Jardines Maternales', 31),
(132, 'Centros Culturales', 32),
(133, 'Declaración Reconocimientos Distinciones', 32),
(134, 'Eventos', 32),
(135, 'Monumentos Ornamentos Esculturas Símbolos Museos', 32),
(136, 'Patrimonio Histórico Municipal', 33),
(137, 'Preservación del Patrimonio', 33),
(138, 'Parque de Reserva Barrancas', 33),
(139, 'Regulación', 34),
(140, 'Régimen Turístico', 35),
(141, 'PAR', 36),
(142, 'Programas de Trabajo Gestión y Promoción', 36),
(143, 'Programas de Desarrollo Municipal', 36),
(144, 'Programas Educativos', 36),
(145, 'Programa Compre Social Maipucino', 36),
(146, 'Adhesiones', 36),
(147, 'Acuerdos', 36),
(148, 'Convenios', 36),
(149, 'Regulaciones', 37),
(150, 'Regulación ', 38),
(151, 'Consultivos', 39),
(152, 'Consejos Departamentales', 39),
(153, 'Consejo de Seguridad Vial Tránsito y Transporte', 39),
(154, 'Consejo de Urbanismo y Planificación', 39),
(155, 'Reglamentación', 40),
(156, 'Niñez y adolescencia', 41),
(157, 'Regulación', 42),
(158, 'Ancianidad', 43),
(159, 'Clubes', 44),
(160, 'Polideportivos y Escuelas Deportivas', 44),
(161, 'Declaraciones de Interés Deportivo', 44),
(162, 'Cine Imperial', 45),
(163, 'Espectáculos Públicos', 45),
(164, 'Locales Bailables - Pub - Recreación', 45),
(165, 'Lugares de Entretenimiento', 45),
(166, 'Reglamentación', 46),
(167, 'Reglamentacion', 48),
(168, 'Ordenanza Tributaria Municipal', 49),
(169, 'Tasas por Servicios a la Propiedad Raíz - Jubilados', 50),
(170, 'Tasas por Uso de Piletas de Natación', 50),
(171, 'Tasas por Construcción', 50),
(172, 'Facilidades de Pago', 51),
(173, 'Congelamiento de Deudas', 51),
(174, 'Reglamentacion', 47);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `titulo`
--

CREATE TABLE `titulo` (
  `id_titulo` int(3) NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `id_libro` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `titulo`
--

INSERT INTO `titulo` (`id_titulo`, `descripcion`, `id_libro`) VALUES
(1, 'Servicios Públicos', 1),
(2, 'Régimen de Infraestructura', 1),
(3, 'Zonificación en General', 2),
(4, 'Zonas Residenciales', 2),
(5, 'Zona de Reserva Urbana', 2),
(6, 'Zona Comercial', 2),
(7, 'Zona de Servicios', 2),
(8, 'Zona Industrial', 2),
(9, 'Zona Rural', 2),
(10, 'Agrupamiento de Vivienda', 2),
(11, 'Otras Disposiciones Complementarias', 2),
(12, 'Comercios', 3),
(13, 'Registros', 3),
(14, 'Industrias', 3),
(15, 'Factibilidad y Habilitación', 3),
(16, 'Protección y Regulación del Medio Ambiente', 4),
(17, 'Salud Pública', 5),
(18, 'Salubridad Pública', 5),
(19, 'Seguridad Pública', 5),
(20, 'Ocupación de la Vía Pública', 6),
(21, 'Ordenamiento Vial', 6),
(22, 'Autoridades de la Seguridad Vial', 6),
(23, 'Juzgado de Tránsito', 6),
(24, 'Vendedores Ambulantes', 6),
(25, 'Espacios Públicos', 7),
(26, 'Barrios', 7),
(27, 'Calles Callejones Pasajes', 7),
(28, 'Conformación del Departamento', 7),
(29, 'PROCREAR', 8),
(30, 'Asunto de Interés Comunitario', 8),
(31, 'Régimen Educativo', 9),
(32, 'Régimen de Cultura', 9),
(33, 'Régimen de Patrimonio', 9),
(34, 'Régimen de Culto', 9),
(35, 'Régimen Turístico', 9),
(36, 'Régimen de los Programas de Interés Municipal', 10),
(37, 'Fiscalización y Control', 10),
(38, 'Centro de Integración Comunitaria', 10),
(39, 'Consejos', 10),
(40, 'Comisiones', 10),
(41, 'Régimen de la Familia', 11),
(42, 'Régimen de la Discapacidad', 11),
(43, 'Régimen de la Ancianidad', 11),
(44, 'Actividades Deportivas', 12),
(45, 'Régimen de Esparcimiento', 12),
(46, 'Régimen de Balnearios y Piscinas', 12),
(47, 'Régimen de la Radiodifusión', 12),
(48, 'Asociaciones Uniones Vecinales Entidades', 12),
(49, 'Código Tributario', 13),
(50, 'Tasas', 13),
(51, 'Régimen de los Pagos y Deudas', 13);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`id_libro`);

--
-- Indices de la tabla `ordenanza`
--
ALTER TABLE `ordenanza`
  ADD PRIMARY KEY (`nro_Ord`),
  ADD KEY `id_Tema` (`id_Tema`) USING BTREE;
ALTER TABLE `ordenanza` ADD FULLTEXT KEY `descripcion` (`descripcion`);

--
-- Indices de la tabla `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_Tema`);

--
-- Indices de la tabla `titulo`
--
ALTER TABLE `titulo`
  ADD PRIMARY KEY (`id_titulo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `libro`
--
ALTER TABLE `libro`
  MODIFY `id_libro` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `tema`
--
ALTER TABLE `tema`
  MODIFY `id_Tema` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
--
-- AUTO_INCREMENT de la tabla `titulo`
--
ALTER TABLE `titulo`
  MODIFY `id_titulo` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ordenanza`
--
ALTER TABLE `ordenanza`
  ADD CONSTRAINT `id_tema` FOREIGN KEY (`id_Tema`) REFERENCES `tema` (`id_Tema`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
