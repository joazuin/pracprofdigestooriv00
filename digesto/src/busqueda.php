<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="..\vista\estilo.css" />
		<title>Digesto HCD Maipú</title>
	</head>
	<body>
	<?php
include ("constantes.php");
require_once ('conexion.php');
if (isset($_POST['tBusqueda']))
{
switch ($_POST['tBusqueda']){
				case "1":
				$nroOrd = $_POST['numero'];
				if($nroOrd == ""){
?>
				<script>
				alert("¡ POR FAVOR INGRESE EL NUMERO DE ORDENANZA !");
				location.href = "../index.php";
				</script>
<?php
				}else{
				$query = "SELECT * FROM digesto.ordenanza WHERE nro_Ord = $nroOrd";
				}
				break;
				
				case "2":
				$palabra = $_POST['palabra'];
				if($palabra == ""){
				?>
				<script>
				alert("¡ POR FAVOR INGRESE LA PALABRA A BUSCAR !");
				location.href = "../index.php";
				</script>
<?php				
				}else{
						// modificar con explode para 4 palabras
						//$palabra = $palabra;
						//$query = "SELECT * FROM digesto.ordenanza WHERE Match(descripcion) AGAINST ('*".$palabra."*'"." IN BOOLEAN MODE)";
						
						$palabras = explode(" ", $palabra);
						$query="SELECT * FROM digesto.ordenanza WHERE ";
						$i=0;
						foreach ($palabras as $word) {
							if ($i != 0) $query .= " OR ";
							$query .= " MATCH (descripcion) AGAINST ('".$word."*'  IN BOOLEAN MODE)";
							$i++;
						}
					}
				break;
				case "3":
				$id_tema = $_POST['cbx_tema'];
				if($id_tema == ""){
?>
				<script>
				alert("¡ POR FAVOR SELECCIONE UN TEMA !");
				location.href = "../index.php";
				</script>
<?php				
				}else{
				$query = "SELECT * FROM digesto.ordenanza WHERE id_Tema = '$id_tema'";
				}
				break;
				case "4":
				$fDesde = $_POST['fecha_desde'];
				$fHasta = $_POST['fecha_hasta'];
				if(($fDesde == '')){
				?>
				<script>
				alert("¡ POR FAVOR INGRESE LA FECHA DESDE !");
				location.href = "../index.php";
				</script>
<?php				
				}
                if(($fDesde > $fHasta)){
				?>
				<script>
				alert("¡ LA FECHA DESDE DEBE SER MENOR QUE LA FECHA HASTA!");
				location.href = "../index.php";
				</script>
<?php				
				}
				if(($fHasta == '')){
				$fHasta = date("y")."-".date("m")."-".date("d");
				}
				$query = "SELECT * FROM digesto.ordenanza WHERE sancion BETWEEN  '$fDesde' AND '$fHasta'";
				break;
				default:
				break;
			}
?>
		<div id="top">
			<div id="header">
				<h1>Digesto HCD Maipú</h1>
			</div>
			<div id="topmenu">
			</div>
		</div>
		<div id="contentwrap">
			<div class="cright">
			<h2>Resultado de búsqueda:</h2>
				<div align="center">
		<?php
		$result = mysqli_query($conect,$query) or die("Error: " . $query);
		if ($row = mysqli_fetch_array($result)){ 
		?>				
						<table border = "2" align = "center" >
							<tr align="center">
								<th >
								Nro Ordenanza
								</th>
								<th >
								Año
								</th>
								<th >
								Sancion
								</th>
								<th >
								Descripción
								</th>
								<th >
								Expediente
								</th>
								<th >
								Ver
								</th>
							  </tr>
		<?php 					
			do { 
				echo "<td>".$row["nro_Ord"]."</td>"; 
				echo "<td>".$row["anio"]."</td>"; 
				echo "<td>".$row["sancion"]."</td>"; 
				echo "<td>".$row["descripcion"]."</td>";
				echo "<td>".$row["expte"]."</td>";
				echo "<td> <a target='_blank' href=".$row["url"]."> archivo </a></td>";
				echo "</tr>";
			} while ($row = mysqli_fetch_array($result)); 
		?>
						</table>
		<?php
		} else {
		?>
				<h3>¡ No se ha encontrado ningún registro !<h3>
		<?php			
			}
?>
				
				<button type = "button"  onclick="javascript:window.location = '../index.php'"> Volver </button>
				</div>
				
			</div>
		</div>
		<div id="footer">
			<div class="left">
				Copyright &copy; Digesto HCD Maipú<a href="#"></a>
			</div>
			<div class="right"> 
				Design by Mariela Maser & Leonardo Albornoz
			</div>
		</div>
<?php 
}else{
echo "<script language = 'javascript'>window.location = '../index.php'</script>";
}
?>
	</body>
</html>