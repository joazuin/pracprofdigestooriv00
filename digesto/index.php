<!DOCTYPE HTML>
<html>
	<head>
		<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />-->
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="vista\estilo.css" />
		<link rel="shortcut icon" href="img/favicon.ico" />
        <title>Digesto HCD Maipú</title>
		<!-- script para combos -->
		<script language="javascript" src="js/jquery-3.1.1.min.js"></script>
		<script language="javascript">
			$(document).ready(function(){
				$("#cbx_libro").change(function () {

					$('#cbx_tema').find('option').remove().end().append('<option value="whatever"></option>').val('whatever');
					
					$("#cbx_libro option:selected").each(function () {
						id_libro = $(this).val();
						$.post("src/getTitulo.php", { id_libro: id_libro }, function(data){
							$("#cbx_titulo").html(data);
						});            
					});
				})
			});
			
			$(document).ready(function(){
				$("#cbx_titulo").change(function () {
					$("#cbx_titulo option:selected").each(function () {
						id_titulo = $(this).val();
						$.post("src/getTema.php", { id_titulo: id_titulo }, function(data){
							$("#cbx_tema").html(data);
						});            
					});
				})
			});
			
			function toggle(elemento) {
				if(elemento.value=="1") {
					document.getElementById("dvNro").style.display = "block";
					document.getElementById("dvPalabra").style.display = "none";
					document.getElementById("dvTemas").style.display = "none";
					document.getElementById("dvFechas").style.display = "none";
				}
				if(elemento.value=="2"){
					document.getElementById("dvNro").style.display = "none";
					document.getElementById("dvPalabra").style.display = "block";
					document.getElementById("dvTemas").style.display = "none";
					document.getElementById("dvFechas").style.display = "none";
				}
				if(elemento.value=="3"){
					document.getElementById("dvNro").style.display = "none";
					document.getElementById("dvPalabra").style.display = "none";
					document.getElementById("dvTemas").style.display = "block";
					document.getElementById("dvFechas").style.display = "none";
				}
				if(elemento.value=="4"){
					document.getElementById("dvNro").style.display = "none";
					document.getElementById("dvPalabra").style.display = "none";
					document.getElementById("dvTemas").style.display = "none";
					document.getElementById("dvFechas").style.display = "block";
				}  
			}
		</script>
	  <!-- fin script para combos --> 
	</head>
	<body>
<?php
include ("src\constantes.php");
require_once ('src\conexion.php');
$query = "SELECT * FROM libro ORDER BY descripcion";
$resultado=$mysqli->query($query);
?>
		<div id="top">
			<div id="header">
				<h1>Digesto HCD Maipú</h1>
			</div>
			<div id="topmenu">
<!--
				<form name="login" method="post" action="validalogeo.php">
					<table width="400">
						<tr>
							<td>
							<input type="text" name="usuario" />
							</td>
							<td>
							<input type="password" name="pass" />
							</td>
							<td>
							<input type="submit" value="Ingresar" />
							</td>
						</tr>
					</table>
				</form>
-->
			</div>
		</div>
		<div id="contentwrap">
			<div class="cright">
			<h2>Búsqueda de Ordenanzas</h2>
				<div>
					<form name="busqueda" id = "fBusqueda" method="post" action="src\busqueda.php">
						<table align = "center">
							<tr>
								<td>
									<input type="radio" name="tBusqueda" onclick="toggle(this)" value = "1"/>
									<label for = ""> Numero de Ordenanzas: </label>
									<div id="dvNro" style="display:none">
										<input id = "txtNro" name="numero" type="number" min = "1" />
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="tBusqueda" onclick="toggle(this)" value = "2" />
									<label for = ""> Palabra Clave: </label>
									<div id="dvPalabra" style="display:none">
										<input id = "txtPalabra" name="palabra" type="text" />
									</div>
								</td>	
							</tr>
							<tr>
								<td>
									<input type="radio" name="tBusqueda" onclick="toggle(this)" value = "3" />
									<label for = ""> Tema: </label>
									<div id="dvTemas" style="display:none">
										<div>
											<select name="cbx_libro" id="cbx_libro">
												<option value="0">Seleccionar libro</option>
												<?php 
													while($row = $resultado->fetch_assoc()) { 
												?>
													<option value="<?php echo $row['id_libro']; ?>"><?php echo $row['descripcion']; ?></option>
												<?php 
													}
												?>
											</select>
										</div>
										<div>
											<select name="cbx_titulo" id="cbx_titulo"></select>
										</div>
										<div>
											<select name="cbx_tema" id="cbx_tema"></select>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="radio" name="tBusqueda" onclick="toggle(this)" value = "4" />
									<label for = ""> Fecha: </label>
									<div id = "dvFechas" style="display:none">
										<label for = ""> Desde: </label>
										<input  type="date" name="fecha_desde"/>
										<label for = ""> Hasta: </label>
										<input type="date" name="fecha_hasta"/>
									</div>
								</td>
							</tr>
							<tr>
								<td align = "center">
								<input type="submit" value="Buscar" name="buscar" onclick = "valida();"/>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="left">
				Copyright &copy; Digesto HCD Maipú<a href="#"></a>
			</div>
			<div class="right"> 
				Developed by Leo Albornoz
			</div>
		</div>
	</body>
<!--  eliminado

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

-->
</html>
